## A JXON Library for Demandware

---

This is a library for converting XML String or E4X objects into Javascript and converting JS objects into XML Strings. This library is a replacement for the E4X "XML" object.    

This library is inspired by the Mozilla JXON code found at https://developer.mozilla.org/en-US/docs/JXON.

The code includes a very simple testing framework I put together in order to test JXON.

Enjoy!

The JXON module exposes a single object that you can - by convention - name "JXON" (see examples below).

The JXON object exposes 2 methods: **toJS()** and **toXMLString()**.

**toJS()** accepts an XML string/E4X parameter and returns a JS object.

**toXMLString()** accepts a JS object and returns an XML string.

### Example: toJS() - Convert XML String into JS object.

```
	var JXON = require("./JXON");
	...
	//XML String parameter
	var obj = JXON.toJS("<animal name='Deka'>is my cat</animal>");
	
	Assert.isObject(obj, "obj");			
	Assert.isObject(obj.animal, "obj.animal");
	Assert.areEqual("Deka", obj.animal["@name"]);
	Assert.areEqual("is my cat", obj.animal.keyValue);
	
	//E4X Parameter
	obj = JXON.toJS(<animal name="Deka">
						is my cat
					</animal>);
	
	Assert.isObject(obj, "obj");			
	Assert.isObject(obj.animal, "obj.animal");
	Assert.areEqual("Deka", obj.animal["@name"]);
	Assert.areEqual("is my cat", obj.animal.keyValue);
```

### Example: toXMLString() - Convert JS object into XML String.
```
    var JXON = require("./JXON");
	...
	var s = JXON.toXMLString({
							root: {
							  myboolean: true,
							  myarray: ["Cinema", "Hot dogs", false],
							  myobject: {
							    nickname: "Jack",
							    registration_date: new Date(1995, 11, 25),
							    privileged_user: true
							  },
							  mynumber: 99,
							  mytext: "Hello World!"
							}
						});
	
				
	Assert.isNotNull(s);
	Assert.areEqual("string", typeof s);
	
	/*
	s = <root>
  			<myboolean>true</myboolean>
			<myarray>Cinema</myarray>
  			<myarray>Hot dogs</myarray>
  			<myarray>false</myarray>
  			<myobject>
    			<nickname>Jack</nickname>
    			<registration_date>Mon, 25 Dec 1995 00:00:00 GMT</registration_date>
    			<privileged_user>true</privileged_user>
  			</myobject>
  			<mynumber>99</mynumber>
  			<mytext>Hello World!</mytext>
		</root>
	*/
``` 

The following is a table showing different conversion patterns:

Case | XML | JSON | Javascript Access
------------ | ------------- | ------------
1 | <animal /> |"animal": {} |	myObject.animal
2 | <animal>Deka</animal> | "animal": "Deka"| myObject.animal
3 | <animal name="Deka" /> | "animal": {"@name": "Deka"} | myObject.animal["@name"]
4 | <animal name="Deka">is my cat</animal> |	"animal": { "@name": "Deka", "keyValue": "is my cat" } | myObject.animal["@name"], myObject.animal.keyValue
5 | <animal> <dog>Charlie</dog> <cat>Deka</cat> </animal> | "animal": { "dog": "Charlie", "cat": "Deka" } | myObject.animal.dog, myObject.animal.cat
6 | <animal> <dog>Charlie</dog> <dog>Mad Max</dog> </animal> | "animal": { "dog": ["Charlie", "Mad Max"] }	| myObject.animal.dog[0], myObject.animal.dog[1]
7 | <animal> in my house <dog>Charlie</dog> </animal> | "animal": { "keyValue": "in my house", "dog": "Charlie" }	| myObject.animal.keyValue, myObject.animal.dog
8 | <animal> in my ho <dog>Charlie</dog> use </animal> | "animal": { "keyValue": "in my house", "dog": "Charlie" }	| myObject.animal.keyValue, myObject.animal.dog

### NOTE: 
**E4X is obsolete** (https://developer.mozilla.org/en-US/docs/Archive/Web/E4X)

"Warning: E4X is obsolete. It has been disabled by default for webpages (content) in Firefox 17, disabled by default for chrome in Firefox 20, and has been removed in Firefox 21. Use DOMParser/DOMSerializer or a non-native JXON algorithm instead.""

### WARNING:
Do not create large JS objects in memory. To avoid out-of-memory conditions when dealing with XML streams use the XMLStreamReader and XMLStreamWriter classes.